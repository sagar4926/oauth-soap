package com.kodematrix.oauthtest.services;

import com.kodematrix.oauthtest.utils.SoapOAuthInjectorInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;

@Component
public class SoapCallerService {
    @Autowired
    SoapOAuthInjectorInterceptor oauthInterceptor;

    @Bean
    WebServiceTemplate getWebServiceTemplate() {
        WebServiceTemplate ws = new WebServiceTemplate();
        ws.setDefaultUri("http://127.0.0.1:8080/soap");
        ws.setInterceptors(new ClientInterceptor[]{
                oauthInterceptor
        });
        return ws;
    }
}
