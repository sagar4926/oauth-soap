package com.kodematrix.oauthtest.services;

import com.kodematrix.oauthtest.utils.SoapOAuthInjectorInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class SoapInterceptorService {

    @Bean
    SoapOAuthInjectorInterceptor getSoapOauthInjectorInterceptor() {
        return new SoapOAuthInjectorInterceptor();
    }
}
