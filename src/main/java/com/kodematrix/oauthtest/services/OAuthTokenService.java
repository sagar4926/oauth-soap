package com.kodematrix.oauthtest.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kodematrix.oauthtest.models.OAuthCredential;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class OAuthTokenService {
    private OAuthCredential token;

    private OAuthCredential generateToken() throws IOException, AuthenticationException, NoSuchAlgorithmException, KeyManagementException {
        Date d = new Date();

        SSLContext sslContext = SSLContext.getInstance("SSL");

// set up a TrustManager that trusts everything
        sslContext.init(null, new TrustManager[]{new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                System.out.println("getAcceptedIssuers =============");
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs,
                                           String authType) {
                System.out.println("checkClientTrusted =============");
            }

            public void checkServerTrusted(X509Certificate[] certs,
                                           String authType) {
                System.out.println("checkServerTrusted =============");
            }
        }}, new SecureRandom());

        SSLSocketFactory sf = new SSLSocketFactory(sslContext);
        Scheme httpsScheme = new Scheme("https", 443, sf);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(httpsScheme);
        schemeRegistry.register(new Scheme("http", new PlainSocketFactory(), 80));

// apache HttpClient version >4.2 should use BasicClientConnectionManager
        ClientConnectionManager cm = new SingleClientConnManager(schemeRegistry);
        HttpClient client = new DefaultHttpClient(cm);


        HttpPost httpPost = new HttpPost("http://127.0.0.1:8080/get-token");

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("scope", "password"));
        params.add(new BasicNameValuePair("grant_type", "password"));
        params.add(new BasicNameValuePair("client_id", "CLIENT_ID"));
        params.add(new BasicNameValuePair("client_secret", "CLIENT_SECRET"));
        httpPost.setEntity(new UrlEncodedFormEntity(params));


        UsernamePasswordCredentials creds = new UsernamePasswordCredentials(
                "username", "password"
        );
        httpPost.addHeader(new BasicScheme().authenticate(creds, httpPost, null));

        HttpResponse response = client.execute(httpPost);

        HttpEntity entity = response.getEntity();

        String json = EntityUtils.toString(entity, StandardCharsets.UTF_8);
        ObjectMapper mapper = new ObjectMapper();
        OAuthCredential credential = mapper.readValue(json, OAuthCredential.class);
        credential.setExpiresAt(d, credential.getExpiresAfter());
        return credential;
    }

    public OAuthCredential getCredential() throws IOException, AuthenticationException, KeyManagementException, NoSuchAlgorithmException {
        if (this.token == null) {
            System.out.println("Token is null. Generating");
            this.token = generateToken();
        }
        if (this.token.isTokenExpired()) {
            System.out.println("Token has expired. Generating");
            this.token = generateToken();
        }
        return token;
    }

}
