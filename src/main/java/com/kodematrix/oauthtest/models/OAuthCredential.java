package com.kodematrix.oauthtest.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Calendar;
import java.util.Date;

public class OAuthCredential {
    @JsonProperty("access_token")
    private String accessToken;

    private String token;
    private int expiresAfter;
    private Date expiresAt;

    //Needed for Jackson
    public OAuthCredential() {}

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getExpiresAfter() {
        return expiresAfter;
    }

    public void setExpiresAfter(int expiresAfter) {
        this.expiresAfter = expiresAfter;
    }

    public void setExpiresAt(Date generatedAt, int expiresAfterSeconds) {
        Calendar c = Calendar.getInstance();
        c.setTime(generatedAt);
        c.add(Calendar.SECOND, expiresAfterSeconds - 30);
        this.expiresAt = c.getTime();
    }


    public boolean isTokenExpired() {
        Date d = new Date();
        return d.getTime() > this.expiresAt.getTime();
    }
}
