package com.kodematrix.oauthtest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.ws.client.core.WebServiceTemplate;

import java.util.HashMap;
import java.util.Map;

@RestController
public class MainController {

    @Autowired
    WebServiceTemplate ws;

    // Main call that calls soap request
    @GetMapping("/main")
    Map<String, String> callMainApi() {
        System.out.println("Hit. Main.");
        try {
            ws.marshalSendAndReceive(null);
        } catch (Exception e) {
//            e.printStackTrace();
        }
        System.out.println("Done. Main.");
        Map<String, String> response = new HashMap<>();
        response.put("status", "Success");
        return response;
    }
}
