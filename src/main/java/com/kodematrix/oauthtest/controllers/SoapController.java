package com.kodematrix.oauthtest.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class SoapController {

    //Acts as Soap Request that requires token
    @PostMapping("/soap")
    Map<String, String> get(@RequestHeader(value = "Authorization", required = false) String authorization) {
        System.out.println("Hit. Soap.");
        System.out.println("Token passed: " + authorization);
        Map<String, String> response = new HashMap<>();
        response.put("token", authorization);
        response.put("status", "Soap Request Called");
        return response;
    }
}
