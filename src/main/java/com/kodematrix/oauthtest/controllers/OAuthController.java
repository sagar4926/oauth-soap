package com.kodematrix.oauthtest.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
public class OAuthController {

    //Acts as OAuth Request that provides token
    @PostMapping("get-token")
    Map<String, String> getToken() {
        System.out.println("Hit. Get Token.");
        Date d = new Date();
        Map<String, String> response = new HashMap<>();
        response.put("token", "JWT Token. Generated at " + d.toLocaleString());
        response.put("expiresAfter", "60");
        return response;
    }
}
